﻿using UnityEngine;
using UnityEngine.SceneManagement;

namespace Sprawlopolis
{
    class LoadScene : MonoBehaviour
    {
        [SerializeField]
        private UnityEngine.Object m_SceneObj = null;

        [SerializeField]
        private string m_SceneName = "";

        #region MonoBehaviour
        private void OnValidate()
        {
            if (m_SceneObj != null)
                m_SceneName = m_SceneObj.name;
        }
        #endregion MonoBehaviour

        #region public
        public void Click()
        {
            SceneManager.LoadSceneAsync(m_SceneName);
        }
        #endregion public
    }
}
