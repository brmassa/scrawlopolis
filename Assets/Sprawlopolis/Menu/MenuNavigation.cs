﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis
{
    class MenuNavigation : MonoBehaviour
    {
        [SerializeField]
        private GameObject m_OwnMenu = null;

        [SerializeField]
        private GameObject m_Target = null;

        public void Click()
        {
            m_Target.SetActive(true);
            m_OwnMenu.SetActive(false);
        }
    }
}
