﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis
{
    class Quit : MonoBehaviour
    {
        public void Click()
        {
            Application.Quit();
        }
    }
}
