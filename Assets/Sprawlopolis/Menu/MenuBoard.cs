﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis
{
    [RequireComponent(typeof(Board))]
    internal class MenuBoard : MonoBehaviour
    {
        [SerializeField]
        private List<Core.Card> m_Cards = new List<Core.Card>();

        [SerializeField]
        private Vector2Int m_GridSize = new Vector2Int(10, 10);

        [SerializeField]
        private Tile m_TilePrefab = null;

        private void Start()
        {
            TilesRandom();
        }

        private void TilesRandom()
        {
            for (int x = 0; x < m_GridSize.x; x++)
            {
                for (int y = 0; y < m_GridSize.y; y++)
                {
                    Core.Tile tileData = null;
                    while (tileData == null || (x == 5 && !((tileData.Roads & Core.Directions.north) == Core.Directions.north && (tileData.Roads & Core.Directions.south) == Core.Directions.south)))
                    {

                        var card = m_Cards[Random.Range(0, m_Cards.Count - 1)];
                        tileData = new Core.Tile(card.Tiles[Random.Range(0, card.Tiles.Count - 1)]);
                    }
                    tileData.Position = new Vector2Int(x, y);
                    var tile = Instantiate(m_TilePrefab);
                    tile.Load(GetComponent<Board>(), tileData);
                }
            }
        }
    }
}
