﻿using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

namespace Sprawlopolis
{
    class TestTiles : MonoBehaviour
    {
        [SerializeField]
        private Tile m_TilePrefab = null;

        [SerializeField]
        private Core.Directions m_Direction = Core.Directions.north;

        [SerializeField]
        private CardTestLayout m_CardTestLayout = null;

        [Header("Data")]
        [SerializeField]
        List<Tile> m_Tiles = new List<Tile>();

        Board m_Board = null;

        #region MonoBehaviour
        private void Start()
        {
            m_Board = FindObjectOfType<Board>();
            Load();
        }
        #endregion MonoBehaviour

        [Sirenix.OdinInspector.Button]
        private void Load()
        {
            var PositionMax = new Vector2Int(1, 1);
            var board = FindObjectOfType<Board>();
            Clear();
            for (int c = 0; c < m_CardTestLayout.CardSOs.Count; c++)
            {
                var positionInitial = m_CardTestLayout.Positions[c];
                for (int t = 0; t < m_CardTestLayout.CardSOs[c].Tiles.Count; t++)
                {
                    var position = positionInitial + Core.DirectionsHelper.Position(m_Direction, m_CardTestLayout.CardSOs[c].Tiles[t].Position, PositionMax);
                    var tileData = AddTile(m_CardTestLayout.CardSOs[c].Tiles[t], position, m_Direction);
                    LoadTile(tileData);
                }
            }
        }

        private Core.Tile AddTile(Core.Tile dataOriginal, Vector2Int position, Core.Directions direction)
        {
            var tile = new Core.Tile(dataOriginal);
            tile.Position = position;
            tile.Direction = direction;
            return tile;
        }

        private void LoadTile(Core.Tile data)
        {
            var tile = Instantiate(m_TilePrefab);
            tile.Load(m_Board, data);
            m_Tiles.Add(tile);
        }

        private void Clear()
        {
            foreach (var tile in m_Tiles)
                Destroy(tile.gameObject);
            m_Tiles.Clear();
        }
    }
}
