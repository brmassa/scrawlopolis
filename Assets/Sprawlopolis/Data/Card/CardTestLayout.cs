﻿using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis
{
    [CreateAssetMenu(menuName = "Sprawlopolis/Test/CardTestLayout")]
    class CardTestLayout : ScriptableObject
    {
        public List<Core.Card> CardSOs = new List<Core.Card>();
        public List<Vector2Int> Positions = new List<Vector2Int>();

        [ContextMenu("FillPositions")]
        private List<Vector2Int> FillPositions()
        {
            var list = new List<Vector2Int>();
            var size = (int)Mathf.Sqrt(CardSOs.Count);
            for (int c = 0; c < CardSOs.Count; c++)
                list.Add(new Vector2Int(-c % size, c / size) * 2);
            Positions = list;
            return list;
        }
    }
}
