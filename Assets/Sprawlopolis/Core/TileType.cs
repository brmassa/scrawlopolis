using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    [CreateAssetMenu(menuName = "Sprawlopolis/TileType")]
    public class TileType : ScriptableObject
    {
        [SerializeField]
        string m_Title = "";

        [SerializeField]
        Color m_Color = Color.white;

        [SerializeField]
        GameObject m_Prefab = null;

        public string Title => m_Title;
        public Color Color => m_Color;
        public GameObject Prefab => m_Prefab;

#if UNITY_EDITOR
        private void OnValidate()
        {
            if (m_Prefab != null)
            {
                var renderer = m_Prefab.GetComponentInChildren<MeshRenderer>();
                if (renderer != null && renderer.sharedMaterial != null)
                {
                    renderer.sharedMaterial.color = m_Color;
                    renderer.sharedMaterial.SetColor("_EmissionColor", m_Color);
                }
            }
        }
#endif
    }
}
