using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    [System.Serializable]
    public class Player
    {
        [SerializeField]
        List<Card> m_Cards = new List<Card>();

        public List<Card> Cards { get { return m_Cards; } set { m_Cards = value; } }
    }
}
