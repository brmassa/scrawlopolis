using System;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    public interface IGameManager
    {
        // Properties
        GameStage GameStage { get; }
        List<Tile> Tiles { get; }
        List<Core.Objective> Objectives { get; }
        int TargetPoints { get; }
        int Points { get; }

        // Actions
        Action<Game> OnGameLoaded { get; set; }
        Action<List<Tile>, List<Tile>, ICard, Vector2Int> OnSimulate { get; set; }
        Action<List<Tile>, ICard, Vector2Int> OnPositionSelected { get; set; }

        // Methods
        void Load(Game data);
        void PositionSelected(ICard cardOnBoard, Vector2Int positionInitial);
        void PositionOver(ICard cardOnBoard, Vector2Int positionInitial);
        int SimulationPoints(List<Core.Tile> tilesSimulation);
    }
}