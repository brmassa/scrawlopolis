﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    [CreateAssetMenu(menuName = "Sprawlopolis/ObjectiveCard")]
    public class ObjectiveCard : ScriptableObject
    {

        [SerializeField]
        int m_ObjectivePoints = 1;

        [SerializeField]
        protected string m_Title = "";

        [SerializeField]
        List<Objective> m_Objectives = new List<Objective>();

        public string Title => m_Title;

        [SerializeField]
        public int ObjectivePoints => m_ObjectivePoints;


        [SerializeField]
        public List<Objective> Objectives => m_Objectives;

#if UNITY_EDITOR
        [ContextMenu("Rename")]
        private void Rename()
        {
            var path = UnityEditor.AssetDatabase.GetAssetPath(this);
            UnityEditor.AssetDatabase.RenameAsset(path, string.Format("{0:D2} {1}", m_ObjectivePoints, m_Title));
        }
#endif
    }
}
