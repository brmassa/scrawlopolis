using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    public interface ICard
    {
        Card Data { get; }
        Vector2Int PositionMax { get; }
        List<Tile> Tiles { get; }
        Directions Direction { get; }
    }
}