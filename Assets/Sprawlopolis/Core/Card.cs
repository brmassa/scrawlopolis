﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    [CreateAssetMenu(menuName = "Sprawlopolis/Card")]
    public class Card : ScriptableObject
    {
        [SerializeField]
        string m_Title = "";

        [SerializeField]
        private int m_CardNum = 1;

        [SerializeField]
        List<Tile> m_Tiles = null;

        public List<Tile> Tiles => m_Tiles;

        public Core.Directions Direction { get; set; }

        public List<Tile> Grid(Directions direction)
        {
            return m_Tiles;
        }

#if UNITY_EDITOR
        private void OnValidate()
        {
            string nameNew = m_CardNum + " ";
            string roads = "";
            foreach (var tile in Tiles)
            {
                nameNew += tile.Type.Title.Substring(0, 1);
                if (tile.Roads == 0)
                    roads += "__";
                else
                {
                    if ((tile.Roads & Directions.north) == Directions.north) roads += "N";
                    if ((tile.Roads & Directions.west) == Directions.west) roads += "W";
                    if ((tile.Roads & Directions.south) == Directions.south) roads += "S";
                    if ((tile.Roads & Directions.east) == Directions.east) roads += "E";
                }
            }
            //name = nameNew;
            m_Title = nameNew + " " + roads;
            if (this.name != m_Title)
                UnityEditor.AssetDatabase.RenameAsset(UnityEditor.AssetDatabase.GetAssetPath(this), m_Title);
        }
#endif

#if UNITY_EDITOR
        [ContextMenu("Rename")]
        private void Rename()
        {
            var path = UnityEditor.AssetDatabase.GetAssetPath(this);
            List<string> names = new List<string>();
            for (int i = 0; i < m_Tiles.Count; i++)
                names.Add(m_Tiles[i].Type.Title.Substring(0, 1));
            UnityEditor.AssetDatabase.RenameAsset(path, string.Format("Card {0} {1}", m_Title, string.Join("", names)));
        }
#endif
    }
}
