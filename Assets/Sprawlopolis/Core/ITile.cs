using UnityEngine;

namespace Sprawlopolis.Core
{
    public interface ITile
    {
        TileType Type { get; }
        Vector2Int Position { get; }
        Directions Roads { get; }
        Directions Direction { get; }
    }
}