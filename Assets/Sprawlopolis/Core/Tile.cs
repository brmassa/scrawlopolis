using UnityEngine;
using Sirenix.OdinInspector;

namespace Sprawlopolis.Core
{
    [System.Serializable]
    public class Tile : ITile
    {
        [SerializeField]
        [HideLabel]
        [HorizontalGroup]
        [GUIColor("ColorType")]
        Vector2Int m_Position = Vector2Int.zero;

        [SerializeField]
        [HideLabel]
        [HorizontalGroup]
        [GUIColor("ColorType")]
        Directions m_Direction = Directions.north;

        [SerializeField]
        [HideLabel]
        [GUIColor("ColorType")]
        [AssetsOnly]
        TileType m_Type = null;

        [SerializeField]
        [HideLabel]
        [EnumToggleButtons]
        [GUIColor("ColorType")]
        Directions m_Roads;

        static public readonly Vector2Int[] Neighbours = new Vector2Int[]
        {
            new Vector2Int(1, 0),
            new Vector2Int(0, 1),
            new Vector2Int(0, -1),
            new Vector2Int(-1, 0),
        };

        public Tile(TileType type)
        { 
            m_Type = Type;}

        public Tile(Tile data)
        {
            m_Type = data.Type;
            m_Direction = data.Direction;
            m_Position = data.Position;
            m_Roads = data.Roads;
        }

        #region public
        public TileType Type => m_Type;
        public Directions Direction { get { return m_Direction; } set { m_Direction = value; } }
        public Vector2Int Position { get { return m_Position; } set { m_Position = value; } }
        public Directions Roads { get { return m_Roads; } set { m_Roads = value; } }

        public Vector2Int PositionDirection(Directions direction = Core.Directions.north)
        {
            return Position;
        }

        public Vector2Int[] MyNeighbours
        {
            get
            {
                var positions = new Vector2Int[Tile.Neighbours.Length];
                for (int p = 0; p < Neighbours.Length; p++)
                    positions[p] = Neighbours[p] + Position;
                return positions;
            }
        }
        #endregion public

        private Color ColorType()
        {
            if (m_Type)
                return m_Type.Color;
            return Color.white;
        }
    }
}
