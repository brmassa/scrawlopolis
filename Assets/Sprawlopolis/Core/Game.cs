﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    [System.Serializable]
    public class Game
    {
        [SerializeField]
        int m_ObjetivePoints = 0;

        [SerializeField]
        int m_PlayerCards = 3;

        [SerializeField]
        List<Player> m_Players = new List<Player>();

        [SerializeField]
        List<Card> m_Cards = new List<Card>();

        [SerializeField]
        List<Tile> m_Tiles = new List<Tile>();

        [SerializeField]
        List<Objective> m_Objective = new List<Objective>();

        public int ObjetivePoints { get { return m_ObjetivePoints; } set { m_ObjetivePoints = value; } }
        public int PlayerCards { get { return m_PlayerCards; } set { m_PlayerCards = value; } }
        public List<Player> Players { get { return m_Players; } set { m_Players = value; } }
        public List<Tile> Tiles { get { return m_Tiles; } set { m_Tiles = value; } }
        public List<Card> Cards { get { return m_Cards; } set { m_Cards = value; } }
        public List<Objective> Objectives { get { return m_Objective; } set { m_Objective = value; } }
    }
}
