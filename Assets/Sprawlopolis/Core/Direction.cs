using UnityEngine;

namespace Sprawlopolis.Core
{
    [System.Flags]
    public enum Directions
    {
        //none = 0,
        north = 1,
        west = 2,
        south = 4,
        east = 8,
    }
    
    public static class DirectionsHelper
    {

        public static Vector2Int Position(Directions direction, Vector2Int positionOriginal, Vector2Int max)
        {
            return direction == Directions.north
             ? new Vector2Int(positionOriginal.x, positionOriginal.y)
             : new Vector2Int(-positionOriginal.x + max.x, -positionOriginal.y + max.y);
        }

        public static int Rotation(Directions direction)
        {
            return direction == Directions.north
            ? 0
            : 180;
        }

        public static int DirectionMultiplier(Directions directions)
        {
            return (int)Mathf.Log((int)directions, 2);
        }
    }
}
