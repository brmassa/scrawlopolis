﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    public abstract class Objective : ScriptableObject
    {
        [SerializeField]
        protected TileType m_TileType = null;

        [SerializeField]
        protected int m_Points = 1;

        [SerializeField]
        protected string m_Title = "";

        [SerializeField]
        protected string m_Description = "";

        #region public
        public virtual string Title => m_Title;
        public virtual string Description => m_Description;
        public virtual int Points => m_Points;

        public virtual int Calculate(List<Tile> tiles)
        {
            return 0;
        }
        #endregion public

        protected List<List<Tile>> TileBlocks(List<Tile> tiles)
        {
            var blocks = new List<List<Tile>>();
            var tilesNonUsed = new List<Tile>();

            for (int t = 0; t < tiles.Count; t++)
                tilesNonUsed.Add(tiles[t]);

            for (int t = tilesNonUsed.Count - 1; t >= 0; t--)
            {
                if (tiles[t].Type == m_TileType)
                    blocks.Add(TileBlock(tiles[t], tilesNonUsed));
            }
            return blocks;
        }

        protected List<Tile> TileBlock(Tile tile, List<Tile> tilesNonUsed)
        {
            var block = new List<Tile>();
            block.Add(tile);
            tilesNonUsed.Remove(tile);

            for (int n = 0; n < tile.MyNeighbours.Length; n++)
            {
                for (int t = tilesNonUsed.Count - 1; t >= 0; t--)
                {
                    if (tilesNonUsed[t].Position == tile.MyNeighbours[n] && tilesNonUsed[t].Type == m_TileType)
                        block.AddRange(TileBlock(tilesNonUsed[t], tilesNonUsed));
                }
            }
            return block;
        }

        protected List<Tile> Neighbours(Tile tile, List<Tile> tiles)
        {
            var neighbours = new List<Tile>();
            for (int n = 0; n < tile.MyNeighbours.Length; n++)
            {
                for (int t = 0; t < tiles.Count; t++)
                {
                    if (tiles[t].Position == tile.MyNeighbours[n])
                        neighbours.Add(tiles[t]);
                }
            }
            return neighbours;
        }
        
        public virtual List<Tile> Solution(List<Tile> tiles)
        {
            return new List<Tile>();
        }
    }
}
