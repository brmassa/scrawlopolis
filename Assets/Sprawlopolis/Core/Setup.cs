﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    [CreateAssetMenu(menuName = "Sprawlopolis/Setup")]
    internal class Setup : ScriptableObject
    {
        [SerializeField]
        int m_ObjectiveCardsQty = 3;

        [SerializeField]
        int m_PlayerCards = 3;

        [SerializeField]
        List<Card> m_Cards = null;

        [SerializeField]
        ObjectiveCard m_ObjectiveCardDifficulty = null;

        [SerializeField]
        List<ObjectiveCard> m_ObjectiveCards = null;

        [SerializeField]
        Core.Directions m_DirectionDefault = Core.Directions.north;

        public Game Create()
        {
            var game = new Game();
            game.PlayerCards = m_PlayerCards;
            Cards(game);
            Objectives(game);
            FirstTile(game);
            return game;
        }

        private void Cards(Game game)
        {
            var cards = new List<Card>();

            for (int i = 0; i < m_Cards.Count; i++)
                cards.Add(m_Cards[i]);

            List.Shuffle(cards);

            for (int i = 0; i < cards.Count; i++)
            {
                cards[i] = Instantiate(cards[i]);
                cards[i].Direction = m_DirectionDefault;
            }
            game.Cards = cards;
        }

        private void Objectives(Game game)
        {
            var objectiveCards = new List<ObjectiveCard>();

            for (int i = 0; i < m_ObjectiveCards.Count; i++)
                objectiveCards.Add(m_ObjectiveCards[i]);

            List.Shuffle(objectiveCards);
            objectiveCards = objectiveCards.GetRange(0, m_ObjectiveCardsQty);
            objectiveCards.Add(m_ObjectiveCardDifficulty);

            var objectives = new List<Objective>();
            for (int i = objectiveCards.Count - 1; i >= 0; i--)
            {
                game.ObjetivePoints += objectiveCards[i].ObjectivePoints;

                for (int o = 0; o < objectiveCards[i].Objectives.Count; o++)
                    objectives.Add(objectiveCards[i].Objectives[o]);
            }

            game.Objectives = objectives;
        }

        private void FirstTile(Game game)
        {
            var card = game.Cards[0];
            game.Cards.Remove(card);
            for (int t = 0; t < card.Tiles.Count; t++)
            {
                card.Tiles[t].Direction = card.Direction;
                game.Tiles.Add(card.Tiles[t]);
            }
        }
    }
}