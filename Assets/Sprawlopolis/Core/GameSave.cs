﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    public class GameSave : ScriptableObject
    {
        public Game Game = null;
    }
}
