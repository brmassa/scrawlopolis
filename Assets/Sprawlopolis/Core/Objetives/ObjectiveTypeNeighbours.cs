using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    [CreateAssetMenu(menuName = "Sprawlopolis/Objective/TypeNeighbours")]
    public class ObjectiveTypeNeighbours : Objective
    {
        [SerializeField]
        TileType[] m_NeighbourTypes = new TileType[0];

        #region Objective
        public override string Title => string.Format(m_Title, m_Points, m_TileType.Title);
        public override string Description => string.Format(m_Description, m_Points, m_TileType.Title);

        public override int Calculate(List<Tile> tiles)
        {
            int points = 0;
            for (int t = 0; t < tiles.Count; t++)
            {
                if (tiles[t].Type == m_TileType)
                {
                    var neighbours = Neighbours(tiles[t], tiles);
                    var neighboursAreFromType = TilesAreFromType(neighbours, m_NeighbourTypes);
                    points += (neighbours.Count == neighboursAreFromType) ? m_Points : 0;
                }
            }

            return points;
        }
        #endregion Objective

        private int TilesAreFromType(List<Tile> tiles, TileType[] tileTypes)
        {
            int tilesAreFromType = 0;
            for (int t = 0; t < tiles.Count; t++)
            {
                for (int nt = tileTypes.Length - 1; nt >= 0; nt--)
                {
                    tilesAreFromType += tiles[t].Type == tileTypes[nt] ? 1 : 0;
                }
            }
            return tilesAreFromType;
        }
    }
}
