using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    [CreateAssetMenu(menuName = "Sprawlopolis/Objective/EdgeType")]
    public class ObjectiveEdgeType : Objective
    {
        [SerializeField]
        bool m_AtBorder = false;

        #region Objective
        public override string Title => string.Format(m_Title, m_Points, m_TileType.Title, m_AtBorder ? " " : " NOT ");
        public override string Description => string.Format(m_Description, m_Points, m_TileType.Title, m_AtBorder ? " " : " NOT ");

        public override int Calculate(List<Tile> tiles)
        {
            return Solution(tiles).Count * m_Points;
        }
        #endregion Objective

        private bool BlockInBorder(Tile tile, List<Tile> tiles)
        {
            return Neighbours(tile, tiles).Count < 4;
        }

        public override List<Tile> Solution(List<Tile> tiles)
        {
            var solution = new List<Tile>();
            for (int t = 0; t < tiles.Count; t++)
            {
                if (tiles[t].Type == m_TileType)
                {
                    var tileInBorder = BlockInBorder(tiles[t], tiles);
                    if ((tileInBorder && m_AtBorder) || (!tileInBorder && !m_AtBorder))
                        solution.Add(tiles[t]);
                }
            }

            return solution;
        }
    }
}
