using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    [CreateAssetMenu(menuName = "Sprawlopolis/Objective/BlockType")]
    public class ObjectiveBlockType : Objective
    {
        #region Objective
        public override string Title => string.Format(m_Title, m_Points, m_TileType.Title);
        public override string Description => string.Format(m_Description, m_Points, m_TileType.Title);

        public override int Calculate(List<Tile> tiles)
        {
            return Solution(tiles).Count * m_Points;
        }

        public override List<Tile> Solution(List<Tile> tiles)
        {
            var solution = new List<Tile>();
            int qty = 0;
            var blocks = TileBlocks(tiles);
            for (int b = 0; b < blocks.Count; b++)
            {
                if (Mathf.Max(qty, blocks[b].Count) > qty)
                {
                    qty = Mathf.Max(qty, blocks[b].Count);
                    solution = blocks[b];
                }
            }
            return solution;
        }
        #endregion Objective
    }
}
