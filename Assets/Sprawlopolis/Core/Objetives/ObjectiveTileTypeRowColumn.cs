using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    [CreateAssetMenu(menuName = "Sprawlopolis/Objective/TileTypeRowColumn")]
    public class ObjectiveTileTypeRowColumn : Objective
    {
        public enum Operator
        {
            GreaterThan,
            LessThan
        }

        [SerializeField]
        int m_Qty = 1;

        [SerializeField]
        Operator m_Operator = Operator.GreaterThan;

        #region Objective
        public override string Title => string.Format(m_Title, m_Qty);
        public override string Description => string.Format(m_Description, m_Qty);

        public override int Calculate(List<Tile> tiles)
        {
            var list = new Dictionary<Vector2Int, Tile>();
            var dimension = new RectInt();
            for (int t = 0; t < tiles.Count; t++)
            {
                dimension.xMin = Mathf.Min(dimension.xMin, tiles[t].Position.x);
                dimension.yMin = Mathf.Min(dimension.yMin, tiles[t].Position.y);

                dimension.xMax = Mathf.Max(dimension.xMax, tiles[t].Position.x);
                dimension.yMax = Mathf.Max(dimension.yMax, tiles[t].Position.y);

                list.Add(tiles[t].Position, tiles[t]);
            }

            int points = 
                 Traverse(list, false, dimension.xMin, dimension.xMax, dimension.yMin, dimension.yMax) 
                + Traverse(list, true, dimension.yMin, dimension.yMax, dimension.xMin, dimension.xMax)
                ;

            return points;
        }
        #endregion Objective

        private int Traverse(Dictionary<Vector2Int, Tile> list, bool invert, int xMin, int xMax, int yMin, int yMax)
        {
            int points = 0;
            for (int x = xMin; x <= xMax; x++)
            {
                var qty = 0;
                for (int y = yMin; y <= yMax; y++)
                {
                    Tile tile = null;
                    var position = invert ? new Vector2Int(y, x) : new Vector2Int(x, y);
                    list.TryGetValue(position, out tile);
                    if (tile != null && tile.Type == m_TileType)
                        qty++;
                }
                points += m_Operator == Operator.GreaterThan ? (qty > m_Qty ? m_Points : 0) : (qty < m_Qty ? m_Points : 0);
            }
            return points;
        }
    }
}
