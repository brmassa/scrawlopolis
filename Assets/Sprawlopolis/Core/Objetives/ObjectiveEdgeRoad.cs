using UnityEngine;

namespace Sprawlopolis.Core
{
    [CreateAssetMenu(menuName = "Sprawlopolis/Objective/EdgeRoad")]
    public class ObjectiveEdgeRoad : Objective
    {
    }
}
