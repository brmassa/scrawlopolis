using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis.Core
{
    [CreateAssetMenu(menuName = "Sprawlopolis/Objective/TileType")]
    public class ObjectiveTileType : Objective
    {
        #region Objective
        public override string Title => string.Format(m_Title, m_Points, m_TileType.Title);
        public override string Description => string.Format(m_Description, m_Points, m_TileType.Title);

        public override int Calculate(List<Tile> tiles)
        {
            return Solution(tiles).Count * m_Points;
        }

        public override List<Tile> Solution(List<Tile> tiles)
        {
            var solution = new List<Tile>();
            for (int t = 0; t < tiles.Count; t++)
                if (tiles[t].Type == m_TileType)
                    solution.Add(tiles[t]);

            return solution;
        }
        #endregion Objective
    }
}
