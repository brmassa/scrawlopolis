using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis
{
    internal class GameSetup : MonoBehaviour
    {
        [SerializeField]
        GameManager m_GameManager = null;

        [SerializeField]
        Core.Setup m_Setup = null;

        [SerializeField]
        int m_players = 1;

        public void Start()
        {
            var game = m_Setup.Create();
            for (int i = 0; i < m_players; i++)
            {
                var player = new Core.Player();
                PlayerCardAdd(game, player);
                game.Players.Add(player);
            }
            m_GameManager.Load(game);
        }

        private void PlayerCardAdd(Core.Game game, Core.Player player)
        {
            for (int c = 0; c < game.PlayerCards; c++)
            {
                var card = game.Cards[0];
                game.Cards.RemoveAt(0);
                player.Cards.Add(card);
            }
        }
    }
}
