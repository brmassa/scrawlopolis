using System;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Sprawlopolis
{
    internal class GameManager : MonoBehaviour, Core.IGameManager
    {
        [SerializeField]
        Board m_Board = null;

        [SerializeField]
        Player m_PlayerPrefab = null;

        [SerializeField]
        Tile m_TilePrefab = null;

        [SerializeField]
        Objective m_ObjectivePrefab = null;

        [SerializeField]
        Transform m_ObjectivePlaceholder = null;

        [Header("Data")]
        [SerializeField]
        Core.Game m_Data = null;

        [SerializeField]
        List<Player> m_Players = null;

        [SerializeField]
        List<Tile> m_Tiles = null;

        [SerializeField]
        Core.GameStage m_GameStage = Core.GameStage.idle;

        #region IGameManager
        public Core.GameStage GameStage => m_GameStage;
        public List<Core.Tile> Tiles => m_Data.Tiles;
        public List<Core.Objective> Objectives => m_Data.Objectives;
        public Action<Core.Game> OnGameOver { get; set; }
        public Action<Core.Game> OnGameLoaded { get; set; }
        public Action<List<Core.Tile>, List<Core.Tile>, Core.ICard, Vector2Int> OnSimulate { get; set; }
        public Action<List<Core.Tile>, Core.ICard, Vector2Int> OnPositionSelected { get; set; }

        public int TargetPoints
        {
            get
            {
                int points = 0;
                for (int o = 0; o < Objectives.Count; o++)
                    points += Objectives[o].Points;
                return points;
            }
        }

        public int Points
        {
            get
            {
                int points = 0;
                for (int o = 0; o < Objectives.Count; o++)
                    points += Objectives[o].Calculate(Tiles);
                return points;
            }
        }

        public int SimulationPoints(List<Core.Tile> tilesSimulation)
        {
            int points = 0;
            for (int o = 0; o < Objectives.Count; o++)
                points += Objectives[o].Calculate(tilesSimulation);
            return points;
        }

        public void Load(Core.Game data)
        {
            m_Data = data;
            LoadTiles();
            LoadPlayers();
            LoadObjetives();

            OnGameLoaded?.Invoke(m_Data);
        }

        public void PositionOver(Core.ICard cardOnBoard, Vector2Int positionInitial)
        {
            var positions = new List<Vector2Int>();
            for (int t = 0; t < cardOnBoard.Tiles.Count; t++)
                positions.Add(positionInitial + Core.DirectionsHelper.Position(cardOnBoard.Direction, cardOnBoard.Tiles[t].Position, cardOnBoard.PositionMax));
            for (int i = 0; i < m_Tiles.Count; i++)
                m_Tiles[i].Hide = positions.Contains(m_Tiles[i].Position);

            Simulate(cardOnBoard, positionInitial);
        }

        public void PositionSelected(Core.ICard cardOnBoard, Vector2Int positionInitial)
        {
            for (int t = 0; t < cardOnBoard.Tiles.Count; t++)
            {
                var tile = new Core.Tile(cardOnBoard.Tiles[t]);
                var position = positionInitial + Core.DirectionsHelper.Position(cardOnBoard.Direction, tile.Position, cardOnBoard.PositionMax);

                TileRemove(m_Data.Tiles, position);

                // Create new 
                tile = AddTile(m_Data.Tiles, tile, position, cardOnBoard.Direction);
                LoadTile(tile);
            }
            CardRemove(cardOnBoard.Data);

            OnPositionSelected?.Invoke(m_Data.Tiles, cardOnBoard, positionInitial);

            DrawCard();

            if (m_Players[0].Cards.Count == 0)
                GameOver();
        }

        public Tile TileFromPosition(Vector2Int position)
        {
            foreach (var tile in m_Tiles)
            {
                if (tile.Position == position)
                    return tile;
            }
            return null;
        }
        #endregion IGameManager

        public Action<int, Player> OnPlayerAdded { get; set; }

        private void LoadPlayers()
        {
            for (int i = 0; i < m_Data.Players.Count; i++)
            {
                var player = Instantiate(m_PlayerPrefab);
                player.Load(this, i, m_Data.Players[i]);
                m_Players.Add(player);

                OnPlayerAdded?.Invoke(i, player);
            }
        }

        private void LoadTiles()
        {
            for (int i = 0; i < m_Data.Tiles.Count; i++)
                LoadTile(m_Data.Tiles[i]);
        }

        private void LoadTile(Core.Tile data)
        {
            var tile = Instantiate(m_TilePrefab);
            tile.Load(m_Board, data);
            m_Tiles.Add(tile);
        }

        private Core.Tile AddTile(List<Core.Tile> list, Core.Tile dataOriginal, Vector2Int position, Core.Directions direction)
        {
            var tile = new Core.Tile(dataOriginal);
            tile.Position = position;
            tile.Direction = direction;
            list.Add(tile);
            return tile;
        }

        private void LoadObjetives()
        {
            for (int i = 0; i < m_Data.Objectives.Count; i++)
            {
                var objective = Instantiate(m_ObjectivePrefab, m_ObjectivePlaceholder);
                objective.Load(this, m_Data.Objectives[i]);
            }
        }

        private void TileRemove(List<Core.Tile> list, Vector2Int position, bool simulate = false)
        {
            for (int i = 0; i < list.Count; i++)
            {
                if (list[i].Position == position)
                {
                    list.RemoveAt(i);
                    break;
                }
            }

            if (simulate)
                return;
            for (int i = 0; i < m_Tiles.Count; i++)
            {
                if (m_Tiles[i].Position == position)
                {
                    Destroy(m_Tiles[i].gameObject);
                    m_Tiles.RemoveAt(i);
                    break;
                }
            }
        }

        private void Simulate(Core.ICard cardOnBoard, Vector2Int positionInitial)
        {
            var list = new List<Core.Tile>();
            for (int i = 0; i < m_Data.Tiles.Count; i++)
                list.Add(m_Data.Tiles[i]);

            for (int t = 0; t < cardOnBoard.Tiles.Count; t++)
            {
                var position = positionInitial + Core.DirectionsHelper.Position(cardOnBoard.Direction, cardOnBoard.Tiles[t].Position, cardOnBoard.PositionMax);
                var tile = new Core.Tile(cardOnBoard.Tiles[t]);
                TileRemove(list, position, true);
                AddTile(list, tile, position, cardOnBoard.Direction);
            }

            OnSimulate?.Invoke(m_Data.Tiles, list, cardOnBoard, positionInitial);
        }

        [ContextMenu("GameOver")]
        private void GameOver()
        {
            OnGameOver?.Invoke(m_Data);
        }

        [ContextMenu("DrawCard")]
        private void DrawCard()
        {
            if (m_Data.Cards.Count > 0)
            {
                var card = m_Data.Cards[0];
                m_Data.Cards.RemoveAt(0);
                m_Players[0].CardAdd(card);
            }
        }

        private void CardRemove(Core.Card card)
        {
            m_Players[0].CardRemove(card);
        }

#if UNITY_EDITOR
        [ContextMenu("CreateSave")]
        private void CreateSave()
        {
            var gameSave = new Core.GameSave();
            gameSave.Game = m_Data;
            AssetDatabase.CreateAsset(gameSave, AssetDatabase.GenerateUniqueAssetPath("Assets/GameSave.asset"));
            AssetDatabase.SaveAssets();
            EditorUtility.FocusProjectWindow();
            Selection.activeObject = gameSave;
        }
#endif
    }
}
