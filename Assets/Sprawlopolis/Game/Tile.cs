﻿using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis
{
    [SelectionBase]
    internal class Tile : MonoBehaviour, Core.ITile
    {
        [SerializeField]
        private List<GameObject> m_RoadsPrefab = new List<GameObject>();

        [SerializeField]
        private Vector3 m_HighlightPosition = Vector3.up;

        [Header("Data")]
        [SerializeField]
        private Core.Tile m_Data = null;

        [SerializeField]
        private GameObject m_Tile = null;

        [SerializeField]
        bool m_Hide = false;

        [SerializeField]
        bool m_Highlight = false;

        [SerializeField]
        Vector3 m_PositionOriginal = Vector3.zero;

        #region Core.ITile
        public Core.TileType Type => m_Data.Type;
        public Vector2Int Position => m_Data.Position;
        public Core.Directions Roads => m_Data.Roads;
        public Core.Directions Direction => m_Data.Direction;
        #endregion Core.ITile

        #region public
        static public Vector2Int[] Neighbours => Core.Tile.Neighbours;
        public void Load(Board board, Core.Tile data)
        {
            m_Data = data;
            board.Position(transform, m_Data.Position);

            transform.localRotation = Quaternion.Euler(
                transform.localRotation.x,
                Core.DirectionsHelper.Rotation(Direction),
                transform.localRotation.z);
            m_PositionOriginal = transform.position;
            m_Tile = Instantiate(m_Data.Type.Prefab, transform);

            LoadRoad(Core.Directions.north);
            LoadRoad(Core.Directions.east);
            LoadRoad(Core.Directions.south);
            LoadRoad(Core.Directions.west);

            name = string.Format("Tile {0} {1}", m_Data.Position, m_Data.Type.Prefab.name);
        }

        public bool Hide
        {
            get { return m_Hide; }
            set
            {
                m_Hide = value;
                m_Tile.SetActive(!m_Hide);
            }
        }

        public bool Highlight
        {
            set
            {
                m_Highlight = value;
                transform.position = m_PositionOriginal + (m_Highlight ? m_HighlightPosition : Vector3.zero);
            }
        }

        private void LoadRoad(Core.Directions roadDirection)
        {
            if ((m_Data.Roads & roadDirection) == roadDirection)
                Instantiate(m_RoadsPrefab[(int)Mathf.Log((int)roadDirection, 2)], transform);
        }

        #endregion public
    }
}
