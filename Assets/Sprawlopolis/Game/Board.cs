using UnityEngine;

namespace Sprawlopolis
{
    internal class Board : MonoBehaviour
    {
        [SerializeField]
        Vector2 m_Scale = Vector2.one;

        #region public
        public void Position(Transform transform, Vector2Int position)
        {
            transform.localPosition = Position(position);
        }

        public Vector3 Position(Vector2Int position)
        {
            return new Vector3(position.x * m_Scale.x, 0, position.y * m_Scale.y);
        }
        #endregion public
    }
}
