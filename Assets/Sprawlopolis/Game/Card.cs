using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis
{
    internal class Card : MonoBehaviour, Core.ICard
    {
        [SerializeField]
        Tile m_TilePrefab = null;

        [SerializeField]
        Camera m_Camera = null;

        [SerializeField]
        Transform m_TilePlaceholder = null;

        [Header("Data")]
        [SerializeField]
        Core.Card m_Data = null;

        [SerializeField]
        List<Tile> m_Tiles = new List<Tile>();

        [SerializeField]
        Player m_Player = null;

        [SerializeField]
        Vector2Int m_PositionMax = Vector2Int.zero;

        [SerializeField]
        Board m_Board = null;

        [SerializeField]
        Vector2Int m_Position = Vector2Int.zero;

        #region MonoBehaviour
        private void OnDestroy()
        {
            m_Player.PositionSelected -= PositionSelected;
            m_Player.PositionIsOver -= PositionSelected;
            m_Player.CardRotated -= CardRotated;
        }
        #endregion MonoBehaviour

        #region ICard
        public Core.Card Data => m_Data;
        public Vector2Int PositionMax => m_PositionMax;
        public List<Core.Tile> Tiles => m_Data.Tiles;
        public Core.Directions Direction => m_Data.Direction;
        #endregion ICard

        #region public
        public Vector2Int Position => m_Position;
        public Camera Camera => m_Camera;

        public void Load(int id, Board board, Player player, Core.Card data, Vector2Int position, Core.Directions direction, bool onCardList)
        {
            m_Data = data;
            m_Player = player;
            m_Board = board;
            m_Camera.enabled = onCardList;
            m_Position = position;

            if (m_Player)
            {
                m_Player.PositionSelected += PositionSelected;
                m_Player.PositionIsOver += PositionSelected;
                m_Player.CardRotated += CardRotated;
                m_Data.Direction = player.Direction;
            }
            else
            {
                m_Data.Direction = direction;
            }

            int x = 0;
            int y = 0;

            for (int i = 0; i < m_Data.Tiles.Count; i++)
            {
                var tile = Instantiate(m_TilePrefab, m_TilePlaceholder);
                m_Data.Tiles[i].Direction = m_Data.Direction;
                tile.Load(m_Board, m_Data.Tiles[i]);
                m_Tiles.Add(tile);

                x = Mathf.Max(x, m_Data.Tiles[i].Position.x);
                y = Mathf.Max(y, m_Data.Tiles[i].Position.y);
            }
            m_PositionMax = new Vector2Int(x, y);
            PositionSelected(m_Position);
            CardRotated(direction);


            name = string.Format("Card {0}", m_Data.name);
        }
        #endregion public

        private void PositionSelected(Vector2Int position)
        {
            m_Position = position;
            if (!m_Camera.enabled)
                transform.localPosition = m_Board.Position(m_Position);
        }

        private void CardRotated(Core.Directions direction)
        {
            m_Data.Direction = direction;
            m_TilePlaceholder.localRotation = Quaternion.Euler(
            m_TilePlaceholder.localRotation.x,
            Core.DirectionsHelper.Rotation(direction),
            m_TilePlaceholder.localRotation.z);

            //var position = direction.Position(Vector2Int.zero, m_PositionMax);
            var position = Core.DirectionsHelper.Position(direction, Vector2Int.zero, m_PositionMax);
            m_Board.Position(m_TilePlaceholder, position);
        }
    }
}
