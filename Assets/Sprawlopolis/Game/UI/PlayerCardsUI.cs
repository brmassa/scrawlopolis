﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis
{
    internal class PlayerCardsUI : MonoBehaviour
    {
        [SerializeField]
        ButtonCard m_ButtonCardPrefab = null;

        [SerializeField]
        Transform m_ButtonCardPlaceholder = null;

        [SerializeField]
        Dictionary<Card, ButtonCard> m_List = new Dictionary<Card, ButtonCard>();

        #region public
        public void CardAdd(Player player, Card card)
        {
            var buttonCard = Instantiate(m_ButtonCardPrefab, m_ButtonCardPlaceholder);
            buttonCard.Load(player, card);
            m_List.Add(card, buttonCard);
        }

        public void CardRemove(Card card)
        {
            Destroy(m_List[card].gameObject);
            m_List.Remove(card);
        }
        #endregion public
    }
}