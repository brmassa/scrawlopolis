using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Sprawlopolis
{
    internal class ObjectiveTarget : MonoBehaviour
    {
        [SerializeField]
        GameManager m_GameManager = null;

        [SerializeField]
        TextMeshProUGUI m_TargetPoints = null;

        [SerializeField]
        TextMeshProUGUI m_Points = null;

        [SerializeField]
        TextMeshProUGUI m_DeltaPoints = null;

        #region MonoBehaviour
        private void OnEnable()
        {
            m_GameManager.OnGameLoaded += OnGameLoaded;
            m_GameManager.OnPositionSelected += OnPositionSelected;
            m_GameManager.OnSimulate += OnSimulate;
        }
        #endregion MonoBehaviour

        private void OnGameLoaded(Core.Game game)
        {
            m_TargetPoints.text = m_GameManager.TargetPoints.ToString();
            Calculate();
        }

        private void OnSimulate(List<Core.Tile> tilesGame, List<Core.Tile> tilesSimulation, Core.ICard cardOnBoard, Vector2Int positionInitial)
        {
            int points = m_GameManager.SimulationPoints(tilesSimulation) - m_GameManager.Points;
            m_DeltaPoints.text = string.Format(points > 0 ? "+{0}" : "{0}", points);
        }

        private void OnPositionSelected(List<Core.Tile> tiles, Core.ICard cardOnBoard, Vector2Int positionInitial)
        {
            Calculate();
        }

        private void Calculate()
        {
            m_Points.text = m_GameManager.Points.ToString();
            m_DeltaPoints.text = "";
        }

    }
}
