﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis
{
    internal class ButtonRotate : MonoBehaviour
    {
        [SerializeField]
        private GameManager m_GameManager = null;

        [Header("Data")]
        [SerializeField]
        private Player m_Player = null;

        #region MonoBehaviour
        private void OnEnable()
        {
            m_GameManager.OnPlayerAdded += PlayerAdded;
        }

        private void OnValidate()
        {
            if (!m_GameManager)
                m_GameManager = FindObjectOfType<GameManager>();
        }
        #endregion MonoBehaviour

        #region public
        public void Click()
        {
            m_Player.CardRotate(m_Player.Direction);
        }
        #endregion public

        private void PlayerAdded(int id, Player player)
        {
            if (id == 0)
            {
                m_Player = player;
            }
        }
    }
}