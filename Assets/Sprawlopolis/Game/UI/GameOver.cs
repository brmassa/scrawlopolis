using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using TMPro;

namespace Sprawlopolis
{
    internal class GameOver : MonoBehaviour
    {
        [SerializeField]
        private string m_WinMessage = "Win";

        [SerializeField]
        private string m_LoseMessage = "Lost";

        [SerializeField]
        private TextMeshProUGUI m_Message = null;

        #region public
        public void Load(GameManager gameManager)
        {
            m_Message.text = gameManager.TargetPoints <= gameManager.Points ? m_WinMessage : m_LoseMessage;
        }

        public void Click()
        {
            Debug.Log("Exit");
            SceneManager.LoadScene(0);
        }
        #endregion public
    }
}