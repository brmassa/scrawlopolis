using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis
{
    internal class GameOverListener : MonoBehaviour
    {
        [SerializeField]
        private GameManager m_GameManager = null;

        [SerializeField]
        private GameOver m_GameOverPrefab = null;

        #region MonoBehaviour
        private void OnEnable()
        {
            m_GameManager.OnGameOver += OnGameOver;
        }
        #endregion MonoBehaviour

        private void OnGameOver(Core.Game game)
        {
            var dialog = Instantiate(m_GameOverPrefab, transform);
            dialog.Load(m_GameManager);
        }
    }
}