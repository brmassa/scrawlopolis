using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Sprawlopolis
{
    internal class Objective : MonoBehaviour
    {
        [SerializeField]
        TextMeshProUGUI m_Points = null;

        [SerializeField]
        TextMeshProUGUI m_SimulationPoints = null;

        [SerializeField]
        TextMeshProUGUI m_Title = null;

        [SerializeField]
        TextMeshProUGUI m_Description = null;

        [Header("Data")]
        [SerializeField]
        Core.IGameManager m_GameManager = null;

        [SerializeField]
        Core.Objective m_Data = null;

        [SerializeField]
        List<Core.Tile> m_SolutionTiles = new List<Core.Tile>();

        #region public
        public void Load(Core.IGameManager gameManager, Core.Objective data)
        {
            m_GameManager = gameManager;
            m_GameManager.OnGameLoaded += OnGameLoaded;
            m_GameManager.OnSimulate += OnSimulate;
            m_GameManager.OnPositionSelected += OnPositionSelected;

            m_Data = data;
            m_Title.text = m_Data.Title;
            m_Description.text = m_Data.Description;
            m_Points.text = 0.ToString();
            m_SimulationPoints.text = "";

            name = string.Format("Objective {0}", m_Data.Title);
        }

        public void HightlightTiles()
        {
            m_SolutionTiles = m_Data.Solution(m_GameManager.Tiles);
            foreach (var tileData in m_SolutionTiles)
            {
                var tile = (m_GameManager as GameManager).TileFromPosition(tileData.Position);
                tile.Highlight = true;
            }
        }

        public void UnHightlightTiles()
        {
            var solutionTiles = m_Data.Solution(m_GameManager.Tiles);
            foreach (var tileData in m_SolutionTiles)
            {
                var tile = (m_GameManager as GameManager).TileFromPosition(tileData.Position);
                tile.Highlight = false;
            }

        }
        #endregion public

        private void OnGameLoaded(Core.Game game)
        {
            OnPositionSelected(game.Tiles, null, Vector2Int.zero);
        }

        private void OnSimulate(List<Core.Tile> tilesGame, List<Core.Tile> tilesSimulation, Core.ICard cardOnBoard, Vector2Int positionInitial)
        {
            var value = m_Data.Calculate(tilesSimulation) - m_Data.Calculate(tilesGame);
            m_SimulationPoints.text = string.Format(value > 0 ? "+{0}" : "{0}", value);
        }

        private void OnPositionSelected(List<Core.Tile> tiles, Core.ICard cardOnBoard, Vector2Int positionInitial)
        {
            m_Points.text = m_Data.Calculate(tiles).ToString();
            m_SimulationPoints.text = "";
        }
    }
}
