﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Sprawlopolis
{
    class ButtonCard : MonoBehaviour
    {
        [SerializeField]
        RawImage m_SpriteCamera = null;

        [SerializeField]
        Button m_Button = null;

        [Header("Data")]
        [SerializeField]
        Card m_Card = null;

        [SerializeField]
        Player m_Player = null;

        #region MonoBehaviour
        private void OnDestroy()
        {
            m_Player.CardSelected -= CardSeleted;
        }
        #endregion MonoBehaviour

        #region public
        public void Load(Player player, Card card)
        {
            m_Player = player;
            m_Player.CardSelected += CardSeleted;
            
            m_Card = card;
            var targetTexture = new RenderTexture(256, 256, 16, RenderTextureFormat.ARGB32);
            m_Card.Camera.targetTexture = targetTexture;
            m_SpriteCamera.texture = targetTexture;
        }

        public void Click()
        {
            m_Player.CardSelect(m_Card.Data, Vector2Int.zero);
        }
        #endregion public

        private void CardSeleted(Core.Card card)
        {
            m_Button.interactable = card != m_Card.Data;
        }
    }
}