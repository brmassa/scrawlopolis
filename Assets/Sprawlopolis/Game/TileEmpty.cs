using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Sprawlopolis
{
    [SelectionBase]
    internal class TileEmpty : MonoBehaviour
    {
        [SerializeField]
        Vector2Int m_Position = Vector2Int.zero;

        [SerializeField]
        Player m_Player = null;

        public Vector2Int Position => m_Position;

        public void Load(Board board, Player player, Vector2Int position)
        {
            m_Position = position;
            m_Player = player;
            board.Position(transform, m_Position);
            name = string.Format("TileEmpty {0}", m_Position);
        }

        void OnMouseEnter()
        {
            m_Player.PositionOver(Position);
        }

        void OnMouseUp()
        {
            m_Player.PositionSelect(Position);
        }
    }
}
