using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Sprawlopolis
{
    internal class Player : MonoBehaviour
    {
        [SerializeField]
        Card m_CardPrefab = null;

        [SerializeField]
        TileEmpty m_TileEmptyPrefab = null;

        [SerializeField]
        private Core.Directions[] m_Directions = { Core.Directions.north, Core.Directions.south };

        [Header("Data")]
        [SerializeField]
        PlayerCardsUI m_PlayerCardsUI = null;

        [SerializeField]
        Core.Directions m_Direction = Core.Directions.north;

        [SerializeField]
        private int m_DirectionId = 0;

        [SerializeField]
        Board m_Board;

        [SerializeField]
        Core.IGameManager m_Game = null;

        [SerializeField]
        Core.Player m_Data = null;

        [SerializeField]
        Card m_CardOnBoard = null;

        [SerializeField]
        Dictionary<Core.Card, Card> m_Cards = new Dictionary<Core.Card, Card>();

        [SerializeField]
        List<TileEmpty> m_TileEmptyList = new List<TileEmpty>();

        #region public
        public Core.Player Data => m_Data;
        public List<Core.Card> Cards => m_Data.Cards;
        public Core.Directions Direction => m_Direction;
        public System.Action<Vector2Int> PositionIsOver { get; set; }
        public System.Action<Vector2Int> PositionSelected { get; set; }
        public System.Action<Core.Card> CardSelected { get; set; }
        public System.Action<Core.Directions> CardRotated { get; set; }

        public void Load(Core.IGameManager game, int id, Core.Player data)
        {
            m_Game = game;
            m_Game.OnPositionSelected += OnPositionSelected;
            m_Data = data;

            name = string.Format("Player {0}", id);

            m_Board = FindObjectOfType<Board>();

            if (id == 0)
            {
                GenerateEmpty();
                LoadPlayerBoard(m_Data);
            }
        }

        public void LoadPlayerBoard(Core.Player player)
        {
            m_PlayerCardsUI = FindObjectOfType<PlayerCardsUI>();
            for (int c = 0; c < player.Cards.Count; c++)
                CardLoad(player.Cards[c], c);
            ChangeLayersRecursively(transform, gameObject.layer);
        }

        public void CardSelect(Core.Card card, Vector2Int positionInitial)
        {
            CardOnBoardDeleted();
            m_CardOnBoard = Instantiate(m_CardPrefab);
            m_CardOnBoard.Load(0, m_Board, this, card, positionInitial, Direction, false);

            CardSelected?.Invoke(card);
        }

        public void CardOnBoard()
        {
            CardOnBoardDeleted();
        }

        public void CardRotate(Core.Directions direction)
        {
            m_Direction = direction;
            CardRotated?.Invoke(direction);

            if (m_CardOnBoard != null)
                m_Game.PositionOver(m_CardOnBoard, m_CardOnBoard.Position);
        }

        public void CardAdd(Core.Card cardData)
        {
            m_Data.Cards.Add(cardData);
            CardLoad(cardData);
        }

        public void CardRemove(Core.Card cardData)
        {
            m_Data.Cards.Remove(cardData);
            m_PlayerCardsUI.CardRemove(m_Cards[cardData]);
            m_Cards.Remove(cardData);
        }

        public void CardMove(Vector2Int positionDelta)
        {
            if (m_CardOnBoard == null) return;
            var tileEmpty = TileEmptyByPosition(positionDelta + m_CardOnBoard.Position);
            if (tileEmpty != null)
                PositionOver(positionDelta + m_CardOnBoard.Position);
        }

        public void GenerateEmpty()
        {
            // Create a empty tile underneath all existing tiles, if not already created
            for (int t = 0; t < m_Game.Tiles.Count; t++)
            {
                bool free = true;
                for (int n = 0; n < m_TileEmptyList.Count; n++)
                {
                    if (m_Game.Tiles[t].Position == m_TileEmptyList[n].Position)
                    {
                        free = false;
                        break;
                    }
                }
                if (free)
                    CreateEmtpy(m_Game.Tiles[t].Position);
            }

            Vector2Int[] cardTilePositions = new Vector2Int[]
            {
                new Vector2Int(0, 0),
                new Vector2Int(0, 1),
                new Vector2Int(1, 0),
                new Vector2Int(1, 1),
            };
            var tileNeighbours = NeighboursPlus(cardTilePositions);

            // Create a empty tile for the neighbours
            for (int t = 0; t < m_Game.Tiles.Count; t++)
            {
                for (int n = 0; n < tileNeighbours.Count; n++)
                {
                    bool free = true;
                    for (int t2 = 0; t2 < m_TileEmptyList.Count; t2++)
                    {
                        if (m_Game.Tiles[t].Position + tileNeighbours[n] == m_TileEmptyList[t2].Position)
                        {
                            free = false;
                            break;
                        }
                    }
                    if (free)
                        CreateEmtpy(m_Game.Tiles[t].Position + tileNeighbours[n]);
                }
            }
        }

        public void PositionOver(Vector2Int position)
        {
            if (m_CardOnBoard != null)
            {
                m_Game.PositionOver(m_CardOnBoard, position);
                m_Board.Position(m_CardOnBoard.transform, position);
                PositionIsOver?.Invoke(position);
            }
        }

        public void PositionSelect(Vector2Int position)
        {
            if (m_CardOnBoard == null)
                return;

            m_CardOnBoard.Data.Direction = m_Direction;
            for (int i = 0; i < m_CardOnBoard.Data.Tiles.Count; i++)
                m_CardOnBoard.Data.Tiles[i].Direction = m_Direction;

            m_Game.PositionSelected(m_CardOnBoard, position);
            PositionSelected?.Invoke(position);
            CardOnBoardDeleted();
        }

        public List<Vector2Int> NeighboursPlus(Vector2Int[] cardTilePositions)
        {
            var list = new List<Vector2Int>();

            for (int t = 0; t < Core.Tile.Neighbours.Length; t++)
            {
                for (int c = 0; c < cardTilePositions.Length; c++)
                {
                    var position = Core.Tile.Neighbours[t] - cardTilePositions[c];
                    if (!list.Contains(position))
                        list.Add(position);
                }
            }
            return list;
        }
        #endregion public

        #region public input
        public void CardNext(InputAction.CallbackContext context)
        {
            if (!context.started) return;
            Core.Card card = null;
            Vector2Int positionInitial = Vector2Int.zero;
            if (m_CardOnBoard)
            {
                var id = Cards.IndexOf(m_CardOnBoard.Data);
                id = ++id % Cards.Count;
                card = Cards[id];
                positionInitial = m_CardOnBoard.Position;
            }
            else
            {
                card = Cards[0];
            }

            CardSelect(card, positionInitial);
        }

        public void CardRotate(InputAction.CallbackContext context)
        {
            if (!context.started) return;
            m_DirectionId = ++m_DirectionId % m_Directions.Length;
            CardRotate(m_Directions[m_DirectionId]);
        }

        public void CardMoveUp(InputAction.CallbackContext context)
        {
            if (!context.started) return;
            CardMove(Vector2Int.up);
        }

        public void CardMoveDown(InputAction.CallbackContext context)
        {
            if (!context.started) return;
            CardMove(Vector2Int.down);
        }

        public void CardMoveRight(InputAction.CallbackContext context)
        {
            if (!context.started) return;
            CardMove(Vector2Int.right);
        }

        public void CardMoveLeft(InputAction.CallbackContext context)
        {
            if (!context.started) return;
            CardMove(Vector2Int.left);
        }

        public void PositionSelect(InputAction.CallbackContext context)
        {
            if (!context.started) return;
            if (m_CardOnBoard == null) return;
            PositionSelect(m_CardOnBoard.Position);
        }
        #endregion public input

        private void CardLoad(Core.Card cardData, int id = 0)
        {
            var card = Instantiate(m_CardPrefab, transform);
            card.Load(id, m_Board, this, cardData, Vector2Int.zero, Direction, true);
            card.transform.localPosition = new Vector3((id + 1) * 100, 0, 0);
            m_Cards.Add(cardData, card);
            m_PlayerCardsUI.CardAdd(this, card);
        }

        private void ChangeLayersRecursively(Transform transform, int layer)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                transform.GetChild(i).gameObject.layer = layer;
                ChangeLayersRecursively(transform.GetChild(i), layer);
            }
        }

        private void CardOnBoardDeleted()
        {
            if (m_CardOnBoard != null)
                Destroy(m_CardOnBoard.gameObject);
            m_CardOnBoard = null;
        }

        private TileEmpty TileEmptyByPosition(Vector2Int position)
        {
            foreach (var tile in m_TileEmptyList)
            {
                if (tile.Position == position)
                    return tile;
            }
            return null;
        }

        private void CreateEmtpy(Vector2Int position)
        {
            var tileEmpty = Instantiate(m_TileEmptyPrefab, transform);
            tileEmpty.Load(m_Board, this, position);
            m_TileEmptyList.Add(tileEmpty);
        }

        private void ClearEmtpy()
        {
            for (int n = 0; n < m_TileEmptyList.Count; n++)
                Destroy(m_TileEmptyList[n]);
            m_TileEmptyList.Clear();
        }

        private void OnPositionSelected(List<Core.Tile> tiles, Core.ICard cardOnBoard, Vector2Int positionInitial)
        {
            GenerateEmpty();
        }
    }
}
